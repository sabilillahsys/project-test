<?php

namespace App\Database\Migrations;
use CodeIgniter\Database\RawSql;
use CodeIgniter\Database\Migration;

class Inventory extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'inventoryid' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'inventoryname' => [
                'type'       => 'VARCHAR',
                'constraint' => '225',
            ],
            'uom' => [
                'type'       => 'VARCHAR',
                'constraint' => '225',
            ],
            'remarks' => [
                'type'       => 'VARCHAR',
                'constraint' => '225',
            ],
            'created_at' => [
                'type'    => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
        ]);
        $this->forge->addKey('inventoryid', true);
        $this->forge->createTable('inventory');
    }

    public function down()
    {
        $this->forge->dropTable('inventory');
    }
}
