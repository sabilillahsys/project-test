<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
        public function run()
    {
        $data = [
            'username' => 'master',
            'name' => 'Hasun',
            'password'    => password_hash('admin',PASSWORD_DEFAULT),
        ];

        // Using Query Builder
        $this->db->table('user')->insert($data);
    }
}
