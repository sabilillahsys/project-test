<?php

namespace App\Controllers;
use App\Models\Inventory;
use App\Controllers\BaseController;

class InventoryController extends BaseController
{
    public function inventory()
    {
        $postModel = new Inventory();

        //pager initialize
        $pager = \Config\Services::pager();

        $data = array(
            'posts' => $postModel->paginate(10, 'post'),
            'pager' => $postModel->pager
        );

        return view('inventory/data_inventory',$data);
    }
    public function create()
    {
        return view('inventory/input_inventory');
    }
    public function store()
    {
        //load helper form and URL
        helper(['form', 'url']);
         
        //define validation
        $validation = $this->validate([
            'inventoryname' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Nama Inventory.'
                ]
            ],
            'uom'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Data UOM.'
                ]
            ],
            'remarks'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Data Remarks.'
                ]
            ],
        ]);

        if(!$validation) {

            //render view with error validation message
            return view('inventory/input_inventory', [
                'validation' => $this->validator
            ]);

        } else {

             //model initialize
            $postModel = new Inventory();
            
            //insert data into database
            $postModel->insert([
                'inventoryname'   => $this->request->getPost('inventoryname'),
                'uom' => $this->request->getPost('uom'),
                'remarks' => $this->request->getPost('remarks'),
            ]);

            //flash message
            session()->setFlashdata('message', 'Post Berhasil Disimpan');

            return redirect()->to(base_url('inventory'));
        }

    }
    public function edit($id)
    {
        $postModel = new Inventory();

        $data = array(
            'post' => $postModel->find($id)
        );
        return view('inventory/edit_inventory', $data);
    }
    public function update($id)
    {
        helper(['form', 'url']);
         
        $validation = $this->validate([
            'inventoryname' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Nama Inventory.'
                ]
            ],
            'uom'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Data UOM.'
                ]
            ],
            'remarks'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Data Remarks.'
                ]
            ],
        ]);

        if(!$validation) {

            //model initialize
            $postModel = new Inventory();

            //render view with error validation message
            return view('inventory/edit_inventory', [
                'post' => $postModel->find($id),
                'validation' => $this->validator
            ]);

        } else {

            //model initialize
            $postModel = new Inventory();
            
            //insert data into database
            $postModel->update($id, [
                'inventoryname'   => $this->request->getPost('inventoryname'),
                'uom' => $this->request->getPost('uom'),
                'remarks' => $this->request->getPost('remarks'),
            ]);

            //flash message
            session()->setFlashdata('message', 'Post Berhasil Diupdate');

            return redirect()->to(base_url('inventory'));
        }

    }
    public function delete($id)
    {
        //model initialize
        $postModel = new Inventory();

        $post = $postModel->find($id);

        if($post) {
            $postModel->delete($id);

            //flash message
            session()->setFlashdata('message', 'Post Berhasil Dihapus');

            return redirect()->to(base_url('inventory'));
        }

    }
}
