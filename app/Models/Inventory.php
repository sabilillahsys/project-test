<?php

namespace App\Models;

use CodeIgniter\Model;

class Inventory extends Model
{
    protected $table = "inventory";
    protected $primaryKey       = 'inventoryid';
    protected $allowedFields = [
        'inventoryid',
        'inventoryname',
        'uom',
        'remarks'
    ];
}
